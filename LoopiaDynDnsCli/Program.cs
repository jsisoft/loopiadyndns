﻿/*
 * Copyright (c) Johan Idstam
 * Using the BSD-3 License.
 * 
 * 2014-07-11
 */
using System;
using LoopiaDynDns;

namespace LoopiaDynDnsCli
{
	class Program
	{
		public static void Main(string[] args)
		{
			//Missing arguments or asking for help?
			//Show some instructions
			
			DnsUpdater updater = null;
			try
			{
				updater = DnsUpdater.ParseCliArgs(args);
			}
			catch(ArgumentException ex)
			{
				Console.WriteLine("There seems to be an argument missing.");
				Console.WriteLine();
				Console.WriteLine(ex.Message);
				Console.WriteLine();
			}
			
			if(updater == null)
			{
				writeHelp();
			}
			else
			{
				var status = updater.Update();
				
				Console.WriteLine(status);
			}
			
			Console.WriteLine();
			Console.WriteLine("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
		
		private static void writeHelp()
		{
			var msg = @"
LoopiaDynDns by Johan Idstam

Here are the arguments this program needs to work:

	-r				This will read your settings from the registry.
					You must run the application with the arguments below
					at least once for this to work.
					
	-u:UserName		Your Loopia username
	-p:Password		Your Loopia password
	-d:Hostname		The hostname you want to update
	-a:IPv4			The updated IPv4 adress
	
	... and optionally:
	
	-s:url			The url to the Loopia DynDNS service
	
";
			Console.WriteLine(msg);
		}

	}
}