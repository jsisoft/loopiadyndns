﻿/*
 * Copyright (c) ${YEAR}, Johan Idstam
 * Using the BSD-3 License.
 * 
 * 2014-07-11
 */
using System;
using NUnit.Framework;

namespace LoopiaDynDns
{
	[TestFixture]
	public class TestUpdater
	{
		[Test]
		public void TestArgumentParsing_EmptyArgs()
		{
			var args = new string[]{};
			var updater = DnsUpdater.ParseCliArgs(args);
			
			Assert.IsNull(updater, "Empty arguments should result in a NULL updater.");
		}
		
		[Test]
		public void TestArgumentParsing_ShowHelp()
		{
			var args = new string[]{"-h"};
			var updater = DnsUpdater.ParseCliArgs(args);
			
			Assert.IsNull(updater, "Empty arguments should result in a NULL updater.");
		}
		
		[Test]
		[ExpectedException(typeof(ArgumentException), "No username supplied.")]
		public void TestArgumentParsing_MissingUsername()
		{
			var args = new string[]{
				//"-u:t_user",
				"-p:t_password",
				"-d:t_hostname",
				"-a:t_ip_adress",
				"-s:t_service_url"
				};
			var updater = DnsUpdater.ParseCliArgs(args);
		}

		[Test]
		[ExpectedException(typeof(ArgumentException), "No password supplied.")]
		public void TestArgumentParsing_MissingPassword()
		{
			var args = new string[]{
				"-u:t_user",
				//"-p:t_password",
				"-d:t_hostname",
				"-a:t_ip_adress",
				"-s:t_service_url"
				};
			var updater = DnsUpdater.ParseCliArgs(args);
			
		}
		
		[Test]
		[ExpectedException(typeof(ArgumentException), "No hostname supplied.")]
		public void TestArgumentParsing_MissingHostname()
		{
			var args = new string[]{
				"-u:t_user",
				"-p:t_password",
				//"-d:t_hostname",
				"-a:t_ip_adress",
				"-s:t_service_url"
				};
			var updater = DnsUpdater.ParseCliArgs(args);
			
		}
		
		[Test]
		[ExpectedException(typeof(ArgumentException), "No IP adress supplied.")]
		public void TestArgumentParsing_MissingIP()
		{
			var args = new string[]{
				"-u:t_user",
				"-p:t_password",
				"-d:t_hostname",
				//"-a:t_ip_adress",
				"-s:t_service_url"
				};
			var updater = DnsUpdater.ParseCliArgs(args);
			
		}
		
		[Test]
		[ExpectedException(typeof(ArgumentException), "No service url supplied.")]
		public void TestArgumentParsing_MissingServiceUrl()
		{
			var args = new string[]{
				"-u:t_user",
				"-p:t_password",
				"-d:t_hostname",
				"-a:t_ip_adress",
				"-s:"
				};
			
			var updater = DnsUpdater.ParseCliArgs(args);
			
		}
		
		[Test]
		public void TestArgumentParser()
		{
			Assert.AreEqual("", DnsUpdater.getArgValue("-a:"));
			Assert.AreEqual("", DnsUpdater.getArgValue("-a"));
			Assert.AreEqual("", DnsUpdater.getArgValue(""));
			Assert.AreEqual("a", DnsUpdater.getArgValue("-a:a"));
			Assert.AreEqual("a", DnsUpdater.getArgValue("-a: a"));
		}
	}
}
