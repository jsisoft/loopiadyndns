﻿/*
 * Copyright (c) Johan Idstam
 * Using the BSD-3 License.
 * 
 * 2014-07-11
 */
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Net;

namespace LoopiaDynDns
{
	/// <summary>
	/// This is where all the magic happens.
	/// </summary>
	public class DnsUpdater
	{
		public string HostName;
		public string IP;
		public string User;
		public string Password;
		public string ServiceUrl = "http://dns.loopia.se/XDynDNSServer/XDynDNS.php?";
		
		public static DnsUpdater ParseCliArgs(string[] args)
		{
			bool useRegistry = false;
			
			//Missing arguments or asking for help?
			//Return null to indicate failed argument parsing.
			if(args.Length == 0 || "-h-?/h/?/help-help".Contains(args[0].ToLower()))
			{				
				return null;
			}
			
			writeRegValue("ServiceUrl", "http://dns.loopia.se/XDynDNSServer/XDynDNS.php?");
			
			var updater = new DnsUpdater();
			foreach(var arg in args)
			{
				switch(arg.ToLower().Substring(0, 2))
				{
					case "-u":
						updater.User = getArgValue(arg);
						writeRegValue("User", updater.User);
						break;
					case "-p":
						updater.Password = getArgValue(arg);
						writeRegValue("Password", updater.Password);
						break;
					case "-d":
						updater.HostName = getArgValue(arg);
						writeRegValue("HostName", updater.HostName);
						break;
					case "-a":
						updater.IP = getArgValue(arg);
						writeRegValue("IP", updater.IP);
						break;
					case "-s":
						updater.ServiceUrl = getArgValue(arg);
						writeRegValue("ServiceUrl", updater.ServiceUrl);
						break;
					case "-r":
						useRegistry = true;
						break;
				}
			}
			
			
			if(useRegistry)
			{
				updater.User = readRegValue("User");
				updater.Password = readRegValue("Password");
				updater.HostName = readRegValue("HostName");
				updater.IP = readRegValue("IP");
				updater.ServiceUrl = readRegValue("ServiceUrl");
			}
			//Make sure all properties are set.
			if(string.IsNullOrEmpty(updater.User)) throw new ArgumentException("No username supplied.");
			if(string.IsNullOrEmpty(updater.Password)) throw new ArgumentException("No password supplied.");	
			if(string.IsNullOrEmpty(updater.HostName)) throw new ArgumentException("No hostname supplied.");	
			if(string.IsNullOrEmpty(updater.IP)) throw new ArgumentException("No IP adress supplied.");	
			if(string.IsNullOrEmpty(updater.ServiceUrl)) throw new ArgumentException("No service url supplied.");	
				
			return updater;
		}
		
		public static string getArgValue(string arg)
		{
			var pos = arg.IndexOf(':') + 1;
			if(pos == 0) return "";
			
			var value = arg.Substring(pos, arg.Length - pos);
			
			return value.Trim();
		}
				
		public string Update()
		{
			var url = string.Format("{0}system=custom&hostname={1}&myip={2}&backmx=yes", ServiceUrl, HostName, IP);
			var wc = new WebClient();
			
			var nc = new NetworkCredential(User, Password);
			wc.Credentials = nc;
			
			
			var status = url + Environment.NewLine + Environment.NewLine + wc.DownloadString(url);
			
			return status;
		}
		
		private static void writeRegValue(string name, string value)
		{
			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software",true);
			var jsiSoftKey = key.CreateSubKey("jsiSoft"); 
			var dnsKey = jsiSoftKey.CreateSubKey("LoopiaDynDNS");

			dnsKey.SetValue(name, value);
		}

		private static string readRegValue(string name)
		{
			RegistryKey key = Registry.CurrentUser.OpenSubKey("Software",true);
			var jsiSoftKey = key.CreateSubKey("jsiSoft"); 
			var dnsKey = jsiSoftKey.CreateSubKey("LoopiaDynDNS");

			return dnsKey.GetValue(name).ToString();
		}
	}
}


/*


GET /XDynDNSServer/XDynDNS.php?
system=custom
&hostname=myhost.com
&myip=11.22.33.44
&backmx=YES
HTTP/1.0
Host: dns.loopia.se
Authorization: Basic username:password
User-Agent: myclient/1.0 me@null.net


*/
